#========================================================================
# NOTES:
#    The octave default is to check for a system supplied package.
#
# OCTIVE override:
#    You can specify to build without octive support by specifying
#      --without octive
#    when invoking rpmbuild.
#========================================================================
%define namespace nds2-client
%define name     nds2-client-swig
%define version  0.16.12
%define release  1.1
%define _prefix  /usr
%define _includedir %{_prefix}/include/%{namespace}
%define _pkgdocdir  %{_defaultdocdir}/%{name}

#------------------------------------------------------------------------
# Defaults
#------------------------------------------------------------------------

%define _use_internal_dependency_generator 0
%define __find_requires %{_builddir}/%{name}-%{version}/config/nds-find-requires

%define check_cmake3  ( 0%{?rhel} && 0%{?rhel} <= 7 )

#========================================================================
#
#  Set up octave symbols as appropriate
#
#========================================================================
%define with_octave !(%{?_without_octave:1}%{!?_without_octave:0})
%define have_octave %(test -e /usr/bin/octave && echo 1 || echo 0)
%define octave_installed ( %{with_octave} && ( 0%{?rhel} && ( 0%{?rhel} <= 7 ) ) && %{have_octave} )

#========================================================================
#
#  Set up python symbols as appropriate
#
#========================================================================
%define check_python2 ( 0%{?__python2:1} || ( 0%{?rhel} && 0%{?rhel} <= 7 ) )
%define check_python3 ( 0%{?__python3:1} || ( 0%{?rhel} >= 7 ) )

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Sanity checks
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#========================================================================
# Main spec file
#========================================================================
Name:           %{name}
Summary:        NDS2 Client interface
Version:        %{version}
Release:        %{release}%{?dist}
License:        GPL
Group:          LSC Software/Data Analysis
Source:         https://software.igwn.org/lscsoft/source/%{name}-%{version}.tar.bz2
Packager:       Edward Maros (ed.maroso@ligo.org)
URL:            https://wiki.ligo.org/Computing/NDSClient
BuildRoot:      %{buildroot}
BuildRequires:  gcc, gcc-c++, glibc
BuildRequires:  libstdc++-static
%if %{check_cmake3}
BuildRequires:  cmake3 >= 3.6
BuildRequires:  cmake >= 2.6
%else
BuildRequires:  cmake >= 3.6
%endif
BuildRequires:  make
BuildRequires:  doxygen, graphviz
BuildRequires:  rpm-build
BuildRequires:  python3-rpm-macros
BuildRequires:  gawk, pkgconfig
BuildRequires:  cyrus-sasl-devel, cyrus-sasl-gssapi
BuildRequires:  libcurl-devel
BuildRequires:  nds2-client-devel >= 0.16.8
BuildRequires:  swig >= 3.0.7
#........................................................................
# Python 2 dependencies
#........................................................................
%if %check_python2
BuildRequires:  python2
BuildRequires:  python-libs
BuildRequires:  python-devel
BuildRequires:  numpy
%endif

#........................................................................
# Python 3 dependencies
#........................................................................
%if %check_python3
BuildRequires:  python%{python3_pkgversion}
BuildRequires:  python%{python3_pkgversion}-libs
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-numpy
%endif
#........................................................................
# Java dependencies
#........................................................................
%if (0%{?rhel} && (0%{?rhel} <= 7 ) )
BuildRequires:  java-1.7.0-openjdk-devel
%else
BuildRequires:  java-1.8.0-openjdk-devel
%endif
#........................................................................
# Octave dependencies
#........................................................................
BuildRequires:  octave-devel
BuildRequires:  octave
Prefix:         %_prefix

%description
The NDS2 client interface allow the user to down-load LIGO data from V1 and V2 LIGO Network Data Servers.

%package -n %{namespace}-java
Group: Development/Scientific
Summary: Java extensions for NDS2
%description -n %{namespace}-java
This provides java wrappers for the nds2 client

%package -n %{namespace}-matlab
Group: Development/Scientific
Summary: MATLAB extensions for NDS2
Requires: %{namespace}-java = %{version}
%description -n %{namespace}-matlab
This provides MATLAB wrappers for the nds2 client

%package -n %{namespace}-octave
Group: Development/Scientific
Summary: Octave extensions for NDS2
%description -n %{namespace}-octave
This provides extensions to octave to access an NDS2 server

%package -n python2-%{namespace}
Group: Development/Scientific
Summary: Python extensions for NDS2
Requires: python2
Requires: numpy
Obsoletes: %{namespace}-python <= 0.15.3
%description -n python2-%{namespace}
This provides python wrappers for the nds2 client

%package -n python%{python3_pkgversion}-%{namespace}
Group: Development/Scientific
Summary: Python extensions for NDS2
Requires: python%{python3_pkgversion}
Requires: python%{python3_pkgversion}-numpy
%description -n python%{python3_pkgversion}-%{namespace}
This provides python wrappers for the nds2 client

%package doc
Group: Development/Scientific
Summary: NDS2 client documentation files
Requires: %{name} = %{version}
BuildArch: noarch
%description doc
This package contains the doxygen formated pages for the nds2-client package and its
high-level language extensions.

%package all
Group:     Development/Scientific
Summary:   NDS2 Client interface
BuildArch: noarch
Requires:  %{name} = %{version}
Requires:  %{name}-doc = %{version}
Requires:  %{namespace}-java = %{version}
Requires:  %{namespace}-matlab = %{version}
%if 0%{?rhel} > 0 && 0%{?rhel} < 8
Requires:  %{namespace}-octave = %{version}
Requires:  python2-%{namespace} = %{version}
%endif
Requires:  python%{python3_pkgversion}-%{namespace} = %{version}
%description all
 The Network Data Server (NDS) is a TCP/IP protocol for retrieving
 online or archived data from thousands of instrument channels at LIGO
 (Laser Interferometer Gravitational-Wave Observatory) sites and data
 analysis clusters.  Version 2 of the protocol includes communication
 with Kerberos and GSSAPI.
 .
 This package installs all NDS2 client packages, including libraries,
 language bindings, binary interface, and documentation.

#----------------------------------------------
# Get onto the fun of building the NDS software
#----------------------------------------------

%prep
%setup -q

%build
%if %check_python2
export PYTHON2_BUILD_OPTS="-DENABLE_SWIG_PYTHON2=yes -DPYTHON2_EXECUTABLE=%{__python2}"
%else
export PYTHON2_BUILD_OPTS="-DENABLE_SWIG_PYTHON2=no"
%endif
%if %check_python3
export PYTHON3_BUILD_OPTS="-DENABLE_SWIG_PYTHON3=yes -DPYTHON3_EXECUTABLE=%{__python3}"
%else
export PYTHON3_BUILD_OPTS="-DENABLE_SWIG_PYTHON3=no"
%endif
%if %octave_installed
export OCTAVE_BUILD_OPTS="-DENABLE_SWIG_OCTAVE=yes -Dpkgoctdatadir=`ocatve-config --m-site-dir` -DOCTAVE_INSTALL_DIR=`octave-config --oct-site-dir`"
%else
export OCTAVE_BUILD_OPTS="-DENABLE_SWIG_OCTAVE=no"
%endif
%if %{check_cmake3}
%{cmake3} \
 ${OCTAVE_BUILD_OPTS} \
 ${PYTHON2_BUILD_OPTS} \
 ${PYTHON3_BUILD_OPTS} \
 -DWITH_SASL=yes \
 -DWITH_GSSAPI=no \
 -DCMAKE_BUILD_TYPE=RelWithDebInfo \
 -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
 .
%else
%{cmake} \
 ${OCTAVE_BUILD_OPTS} \
 ${PYTHON2_BUILD_OPTS} \
 ${PYTHON3_BUILD_OPTS} \
 -DWITH_SASL=yes \
 -DWITH_GSSAPI=no \
 -DCMAKE_BUILD_TYPE=RelWithDebInfo \
 -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
 .
%endif

%install
make install DESTDIR=%{buildroot}

%check
%if %{check_cmake3}
export CTEST_PROGRAM="ctest3"
%else
export CTEST_PROGRAM="ctest"
%endif
${CTEST_PROGRAM} -V %{?_smp_mflags}

#----------------------------------------------
# Remove files that will not be packaged based
# on the arch type
#----------------------------------------------

#----------------------------------------------
# Do the noarch files
#----------------------------------------------

%files doc
%license COPYING
%{_docdir}

#----------------------------------------------
# Handle binary packages
#----------------------------------------------

%files
%license COPYING
%{_includedir}

#------------------------------------------------------------------------
# Java
#------------------------------------------------------------------------
%files -n %{namespace}-java
%license COPYING
%_libdir/java/nds2
%_libdir/%{namespace}/java
%_datadir/%{namespace}/java
%_sysconfdir/%{namespace}/%{namespace}-java.cfg

#------------------------------------------------------------------------
# Octave
#------------------------------------------------------------------------
%if %{octave_installed}
%files -n %{namespace}-octave
%license COPYING
%(octave-config --oct-site-dir)
%_sysconfdir/%{namespace}/%{namespace}-octave.cfg
%endif

#------------------------------------------------------------------------
# Python
#------------------------------------------------------------------------
%if %{check_python2}
%files -n python2-%{namespace}
%license COPYING
%{python2_sitearch}/
%_sysconfdir/%{namespace}/%{namespace}-py2*.cfg
%endif

#------------------------------------------------------------------------
# Python 3
#------------------------------------------------------------------------
%if %{check_python3}
%files -n python%{python3_pkgversion}-%{namespace}
%license COPYING
%{python3_sitearch}/
%_sysconfdir/%{namespace}/%{namespace}-py3*.cfg
%endif

#------------------------------------------------------------------------
# MATLAB
#------------------------------------------------------------------------
%files -n %{namespace}-matlab
%license COPYING
%_datadir/%{namespace}/matlab
%_sysconfdir/%{namespace}/%{namespace}-matlab.cfg

#------------------------------------------------------------------------
# All - metapackage
#------------------------------------------------------------------------
%files all
